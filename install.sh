#!/usr/bin/env bash
#
# Author: Ole Osterthun
# ___version___=0.1
#

# Fail on error and undefined vars
set -eu

# Fail on single failed command in a pipeline
set -o pipefail

# Save global script args

ARGS="$*"

# Main loop
run() {
    if eval "$(hasflag --help -h)"; then
        display_help
        exit 0
    fi

    if eval "$(hasflag --version -v)"; then
        source "./version.sh"
        echo "rwth-dotfiles ${version}"
        exit 0
    fi

    if [[ -z "${ARGS}" ]]; then
        display_help
        exit 0
    fi

    echo "Installing new dotfiles for use at claix23 @ RWTH Aachen University"
    if [[ -f "$HOME/.zshrc" ]]; then
        message info "Found .zshrc"   
    else
        message error "Did not find .zshrc"
        message error "Aborting now..."
        exit 1
    fi
    if [[ -f "$HOME/.bashrc" ]]; then
        message info "Found .bashrc"
    fi
    if [[ -f "$HOME/.bash_profile" ]]; then
        message info "Found .bash_profile"
    fi
    message info "Creating backup of files as BAK.<zshrc,bashrc,bash_profile>"
    cp "$HOME/.zshrc" "$HOME/BAK.zshrc"
    cp "$HOME/.bashrc" "$HOME/BAK.bashrc"
    cp "$HOME/.bash_profile" "$HOME/BAK.bashprofile"

    message info "Copying over the new dotfiles now"
    current_dir=${PWD##*/}
    current_dir=${result:-/}
    if [[ "$current_dir" != "rwth-dotfiles" ]]; then
        message error "Not in the 'rwth-dotfiles' directory."
        message error "Aborting now..."
        exit 1
    fi
    cp "./dot.bashrc" "$HOME/.bashrc"
    cp "./dot.zshrc" "$HOME/.zshrc"
    cp "./dot.bash_prompt" "$HOME/.bash_prompt"
    cp "./dot.aliases" "$HOME/.aliases"
    cp "./dot.functions" "$HOME/.functions"
    cp "./dot.rwth.init-modules" "$HOME/.rwth.init-modules"
    cp "./dot.rwthrc" "$HOME/.rwthrc"
    # Hardcoding current version
    shopt -s extglob
    for file in  $HOME/.{bashrc,zshrc,bash_prompt,aliases,functions,rwth.init-modules,rwthrc}; do
        write_version file
    done
    message info "Copying new files completed without errors"
    message info "Try to log in to the cluster in a parallel session"
    message info "Could you successfully log in?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) message info "Successfully installed the dotfiles."; exit 0;;
            No ) message info "Reverting to your old dotfiles now"; revert_back;;
        esac
    done

}

usage() {
    cat <<EOT
    -h --help           Print this help message.
EOT
}

display_help() {
    cat <<EOT
Usage: bash install.sh

Installs the dotfiles for use at claix23 @ RWTH Aachen University.
EOT
    echo "Options for install.sh:"
    echo -e "$(usage)"
}

hasflag() {
    local flags="$*"
    for var in $ARGS; do
        for flag in $flags; do
            if [ "$var" = "$flag" ]; then
                echo 'true'
                return
            fi
        done
    done
    echo 'false'
}

message() {
    local type=$1
    local text=$2
    if [[ "$type" = "info" ]]; then
        echo "INFO:       ${text}"
    fi
    if [[ "$type" = "error" ]]; then
        echo "ERROR:      ${text}"
    fi
}

revert_back() {
    message info "Restoring dotfiles from the backup"
    cp "$HOME/BAK.zshrc" "$HOME/.zshrc"
    cp "$HOME/BAK.bashrc" "$HOME/.bashrc"
    cp "$HOME/BAK.bash_profile" "$HOME/.bash_profile"
    message info "Try to log in to the cluster in a parallel session"
    message info "Could you successfully log in?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) message info "You have to manually install the files."; exit 0;;
            No ) message error "You broke your system. Do NOT close this ssh session. Ask Google!"; exit 1;;
        esac
    done
}

write_version() {
    local file=$1
    source "./version.sh"
    sed -i "/___version___/c\# ___version___: ${version}" "$1"
}

run $ARGS
