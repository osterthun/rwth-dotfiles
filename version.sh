#!/usr/bin/env bash
#
# Author: Ole Osterthun

# Check if sourced
if (return 0 2>/dev/null) ; then
    : #Everything is fine
else
    echo "This script is only meant to be sourced."
    exit 0
fi

version="0.1.0"