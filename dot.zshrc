#!/bin/zsh
#
# This script is intended to switch to a different shell on the
# RWTH RZ cluster, which only allows zsh as login shell.
# ___version___: 0.1.0

source /usr/local_host/etc/switch_login_shell bash
