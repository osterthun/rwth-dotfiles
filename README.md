# RWTH dotfiles

This repository contains several dotfiles that make my life on the HPC claix23
at RWTH Aachen University easier. Please use on your own risk. If you know
your way around Linux just manually copy the dotfiles or part of them that
are useful for you. I have also created an install script, tailored to the
base configuration at RWTH Aachen University, it will do the following things:
- Backup all current dotfiles which would be overwritten, namely:
    - `.zshrc`
    - `.bashrc`
    - `.bash_profile`
- ohange the default shell from `zsh` to `bash`
- change the command prompt to:
    ```console
    HH:mm [xy123456@host /your/current/path/fully/expaned]$
    ```
    with:
    - `HH:mm`: current time
    - `xy123456`: your TIM ID
- give you access to a variety of new commands/aliases such as:
    - `q`: Showing your currently running and queued jobs
    - `gotojobdir`: changing into a directory based on a job-id
- add the directory `~/bin` to your `PATH`, e.g. making all executable scripts
in that directory accessible from anywhere in your system

After running the install script you will find the following new files in your
`$HOME` directory:
- `.bashrc`
- `.bash_prompt`
- `.aliases`
- `.zshrc`
- `.rwth.init-modules`
- `.rwth-modulesrc`
