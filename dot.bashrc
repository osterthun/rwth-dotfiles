#!/usr/bin/env bash
#
# Author: Ole Osterthun
#
# shellcheck disable=SC1090
#
# Customized ~/.bashrc for use at RWTH HPC Cluster
#
# ___version___: 0.1.0

# For non-interactive shells don't do anything
# INFO: If submitting batch jobs, you may have to explicitly set
# the enironment within the script (e.g. adding $HOME/bin to path)

[[ -z $PS1 ]] && return

# Add `~/bin` and `~/.local/bin` to `PATH`
export PATH="$HOME/.local/bin:$HOME/bin:$PATH"

# Load configuration files
for file in $HOME/.{rwth.init_modules,bash_prompt,aliases,functions,rwthrc}; do
    [[ -r $file ]] && [[ -f $file ]] && source "$file"
done

# Check if .dircolors are available
test -r $HOME/.dircolors && eval "$(dircolors $HOME/.dircolors)"

shopt -s histappend
HISTCONTROL=ignoreboth

HISTSIZE=10000
HISTFILESIZE=20000

shopt -s extglob
